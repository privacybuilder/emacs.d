
    "Emacs outshines all other editing software in approximately the
    same way that the noonday sun does the stars. It is not just bigger
    and brighter; it simply makes everything else vanish."
    
             - Neal Stephenson, "In the Beginning was the Command Line"


# emacs.d

Long ago, this was forked from the emacs-starter-kit. I have taken it a new direction, and IMHO it is much simpler now.
