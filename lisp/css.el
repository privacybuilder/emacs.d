(add-to-list 'auto-mode-alist '("\\.css\\'" . css-mode))

(add-hook 'css-mode-hook
          (lambda ()
            (setq-local css-indent-offset 2)
            (add-hook 'before-save-hook 'prettier-js nil t)))
;(setq css-mode-hook nil)

(add-to-list 'auto-mode-alist '("\\.scss\\'" . scss-mode))
(add-hook 'scss-mode-hook
          (lambda ()
            (setq-local css-indent-offset 2)
            (add-hook 'before-save-hook 'prettier-js nil t)))

(setq scss-mode-hook nil)

  
