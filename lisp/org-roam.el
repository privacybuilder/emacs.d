(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory (concat org-directory "/roam/"))
  (org-roam-db-location (concat org-directory "/roam/roam.db"))
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
      '(("d" "default" plain
	 "%?"
	 :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
	 :unnarrowed t)))
  :bind  (("C-c n l" . org-roam-buffer-toggle)
	  ("C-c n f" . org-roam-node-find)
	  ("C-c n i" . org-roam-node-insert)
	  :map org-mode-map
	  ("C-M-i" . completion-at-point)
	  :map org-roam-dailies-map
	  ("Y" . org-roam-dailies-capture-yesterday)
	  ("T" . org-roam-dailies-capture-tomorrow)
	  )
  ;:bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (require 'org-roam-dailies)
  (org-roam-db-autosync-enable)
)
